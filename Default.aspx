﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p align="center">
        <asp:Label ID="Label1" runat="server" Font-Size="X-Large" Text="Перегляд таблиці з внесеними даними:"></asp:Label>
        <!-- ---------------------------------------------------------------------------------------------------- --->
        
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="id_заявки" DataSourceID="SqlDataSource1" Width="713px" AllowPaging="True" CellPadding="4" ForeColor="#333333" GridLines="None" style="margin-top: 66px" Font-Size="X-Large" HorizontalAlign="Center" OnRowDataBound="GridView1_RowDataBound">
        <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="id_заявки" HeaderText="№" InsertVisible="False" ReadOnly="True" SortExpression="id_заявки" />
                <asp:BoundField DataField="ПІБ" HeaderText="ПІБ" SortExpression="ПІБ" />
                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                <asp:BoundField DataField="Назва_блоку_живлення" HeaderText="БЖ" SortExpression="Назва_блоку_живлення" />
                <asp:BoundField DataField="Назва_процесора" HeaderText="Процесор" SortExpression="Назва_процесора" />
                <asp:BoundField DataField="Назва_доставки" HeaderText="Доставка" SortExpression="Назва_доставки" />
                <asp:BoundField DataField="Адреса" HeaderText="Адреса" SortExpression="Адреса" />
                <asp:BoundField DataField="Дод_інформ" HeaderText="Інфо" SortExpression="Дод_інформ" />
                <asp:BoundField DataField="edit_by" HeaderText="ред." SortExpression="edit_by" />
                <asp:BoundField DataField="edit_time" HeaderText="ред.час" SortExpression="edit_time" />
                <asp:CommandField ShowDeleteButton="False"   
                  ShowEditButton="False" 
                ShowSelectButton="true" buttontype="Image" SelectImageUrl="~/images/selector.png" HeaderText="SEL"/>  
            
                <asp:TemplateField ShowHeader="False" HeaderText="DEL">
                <ItemTemplate>
                    <asp:LinkButton ID="BtnUserDelete"  runat="server" CausesValidation="false" CommandName="Cancel" Text="Видалити" OnClick="BtnUserDelete_Click"
                       OnClientClick="if ( ! UserDeleteConfirmation()) return false;"><img src="/images/garbage.png"/></asp:LinkButton>
                    <script type="text/javascript" language="javascript"> 
                        function UserDeleteConfirmation() {
                            var rowsCount = parseInt(<%=GridView1.SelectedValue %>);
                            if (rowsCount > 0 && rowsCount < 50000)
                            {
                                return confirm("Ви впевнені що хочете видалити запис №" + rowsCount + "?");
                            } else {
                                alert("Спочатку виберіть запис!");
                                return false;
                            }
                            
                        }
                    </script>
                </ItemTemplate>
            </asp:TemplateField>
                
            <asp:TemplateField HeaderText="UPD" ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="BtnUserUpdate" runat="server" CausesValidation="false" CommandName="Update" OnClick="BtnUserUpdate_Click"
                        OnClientClick="if ( ! UserUpdate()) return false;"><img src="/images/update.png" /></asp:LinkButton>
                               <script type="text/javascript" language="javascript"> 
                                   function UserUpdate() {
                                       
                                       var rowsel = 0;
                                           rowsel = parseInt(<%=GridView1.SelectedValue%>);
                                       
                                       if (rowsel>0 && rowsel<50000)
                                   {
                                       
                                       return true;
                                   } else{
                                           alert("Спочатку виберіть запис!");
                                           return false;
                                   }
                               
                        }
                    </script>
                     </ItemTemplate>
            </asp:TemplateField>
            </Columns>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <SortedAscendingCellStyle BackColor="#FDF5AC" />
        <SortedAscendingHeaderStyle BackColor="#4D0000" />
        <SortedDescendingCellStyle BackColor="#FCF6C0" />
        <SortedDescendingHeaderStyle BackColor="#820000" />

    </asp:GridView>

    </p>
    <div align="center">

<!-- ---------------------------------------------------------------------------------------------------- --->
        
        <asp:LoginView ID="LoginView2" runat="server">
            <AnonymousTemplate>
                <p style="color: #FF0000">Ви не можете редагувати та видаляти записи, тому що не авторизовані!
                    <asp:LoginStatus ID="LoginStatus2" runat="server" ForeColor="Lime" LoginText="Увійти" />
                </p>
            </AnonymousTemplate>
        </asp:LoginView>
        
    </div>
    <p align="center">
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" Font-Size="X-Large" HorizontalAlign="Center">
            <Columns>
                <asp:BoundField DataField="Назва_накопичувача" HeaderText="Назва_накопичувача" SortExpression="Назва_накопичувача" />
            </Columns>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <SortedAscendingCellStyle BackColor="#FDF5AC" />
        <SortedAscendingHeaderStyle BackColor="#4D0000" />
        <SortedDescendingCellStyle BackColor="#FCF6C0" />
        <SortedDescendingHeaderStyle BackColor="#820000" />
        </asp:GridView>
        <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource3" Font-Size="X-Large" HorizontalAlign="Center">
            <Columns>
                <asp:BoundField DataField="Назва_ОС" HeaderText="Назва_ОС" SortExpression="Назва_ОС" />
            </Columns>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <SortedAscendingCellStyle BackColor="#FDF5AC" />
        <SortedAscendingHeaderStyle BackColor="#4D0000" />
        <SortedDescendingCellStyle BackColor="#FCF6C0" />
        <SortedDescendingHeaderStyle BackColor="#820000" />
        </asp:GridView>


</p>
    <p>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:WebDB1ConnectionString %>" SelectCommand="SELECT Заявка.id_заявки, Заявка.ПІБ, Заявка.Адреса, Заявка.Email, Заявка.Дод_інформ, Блок_живлення.Назва_блоку_живлення, Доставка.Назва_доставки, Процесор.Назва_процесора, Заявка.edit_by, Заявка.edit_time
FROM Заявка 
INNER JOIN Доставка ON Заявка.Доставка = Доставка.id_доставки
INNER JOIN Блок_живлення ON Заявка.Блок_живлення = Блок_живлення.id_блоку_живлення
INNER JOIN Процесор ON Заявка.Процесор = Процесор.id_процесора"
></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:WebDB1ConnectionString %>" SelectCommand="SELECT Накопичувач.Назва_накопичувача FROM Накопичувач_заявка INNER JOIN Накопичувач ON Накопичувач_заявка.id_накопичувача = Накопичувач.id_накопичувача WHERE (Накопичувач_заявка.id_заявки = @id_заявки)">
        <SelectParameters>
            <asp:ControlParameter ControlID="GridView1" Name="id_заявки" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:WebDB1ConnectionString %>" SelectCommand="SELECT ОС.Назва_ОС FROM ОС_заявка INNER JOIN ОС ON ОС_заявка.id_ОС = ОС.id_ОС WHERE (ОС_заявка.id_заявки = @id_заявки)">
        <SelectParameters>
            <asp:ControlParameter ControlID="GridView1" Name="id_заявки" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</p>
<p align="center">
    <asp:Button ID="Button1" runat="server" BackColor="#FF9933" BorderColor="#660033" Height="30px" PostBackUrl="~/Lab5form.aspx" Text="Заповнити форму опитування" Width="347px" />
</p>
    <p align="center">
        &nbsp;</p>
    <p align="center">
        
</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
</asp:Content>

