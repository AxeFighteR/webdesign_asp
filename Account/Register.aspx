﻿<%@ Page Title="Регистрация" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Account_Register" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1">

    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

    <div class="form-horizontal">
        <h4 align="center">Створення нового облікового запису</h4>
        <hr style="height: 2px" />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group" align="center">
            <div class="col-md-10" align="center">
        <div class="form-group" align="center">
            <asp:Label runat="server" AssociatedControlID="UserName" CssClass="col-md-2 control-label" ID="Label2">Ім&#39;я користувача</asp:Label>
            <div class="col-md-10" align="center">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox runat="server" ID="UserName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName"
                    CssClass="text-danger" ErrorMessage="Введіть ім'я!" ID="RequiredFieldValidator1" />
            </div>
            <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label" ID="Label3">Пароль</asp:Label>
            <div class="col-md-10" align="center">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                    CssClass="text-danger" ErrorMessage="Введіть пароль!" ID="RequiredFieldValidator2" />
            </div>
            <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label" ID="Label4">Підтвердження пароля</asp:Label>
        </div>
                <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="Повторіть введення паролю!" />
                <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="Паролі не співпадають!" />
                <br />
                <asp:Label ID="Label1" runat="server" Text="E-mail"></asp:Label>
                <br />
                <asp:TextBox ID="EmailField" runat="server" required="required"></asp:TextBox>
                <br />
                Стать<br />
                <asp:DropDownList ID="SexField" runat="server">
                    <asp:ListItem>Чоловік</asp:ListItem>
                    <asp:ListItem>Жінка</asp:ListItem>
                </asp:DropDownList>
                <br />
                Дата народження<br />
                Рік
                <asp:DropDownList ID="YearField" runat="server">
                    <asp:ListItem>1990</asp:ListItem>
                    <asp:ListItem>1991</asp:ListItem>
                    <asp:ListItem>1992</asp:ListItem>
                    <asp:ListItem>1993</asp:ListItem>
                    <asp:ListItem>1994</asp:ListItem>
                    <asp:ListItem>1995</asp:ListItem>
                    <asp:ListItem>1996</asp:ListItem>
                    <asp:ListItem>1997</asp:ListItem>
                    <asp:ListItem>1998</asp:ListItem>
                    <asp:ListItem>1999</asp:ListItem>
                    <asp:ListItem>2000</asp:ListItem>
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                </asp:DropDownList>
            &nbsp;Місяць
                <asp:DropDownList ID="MonthField" runat="server">
                    <asp:ListItem Value="01">Січень</asp:ListItem>
                    <asp:ListItem Value="02">Лютий</asp:ListItem>
                    <asp:ListItem Value="03">Березень</asp:ListItem>
                    <asp:ListItem Value="04">Квітень</asp:ListItem>
                    <asp:ListItem Value="05">Травень</asp:ListItem>
                    <asp:ListItem Value="06">Червень</asp:ListItem>
                    <asp:ListItem Value="07">Липень</asp:ListItem>
                    <asp:ListItem Value="08">Серпень</asp:ListItem>
                    <asp:ListItem Value="09">Вересень</asp:ListItem>
                    <asp:ListItem Value="10">Жовтень</asp:ListItem>
                    <asp:ListItem Value="11">Листопад</asp:ListItem>
                    <asp:ListItem Value="12">Грудень</asp:ListItem>
                </asp:DropDownList>
&nbsp;Число
                <asp:DropDownList ID="DateField" runat="server">
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                <br />
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10" align="center">
                <asp:Button runat="server" OnClick="CreateUser_Click" Text="Реєстрація" CssClass="btn btn-default" />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>

