﻿using Microsoft.AspNet.Identity;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using WEBtest;

public partial class Account_Register : Page
{
    

    protected void CreateUser_Click(object sender, EventArgs e)
    {
        //feedbacktodbDataContext userdb = new feedbacktodbDataContext();
        //userinfo userinfotable = new userinfo();
        using (SqlConnection Connection = new SqlConnection(@"Data Source=AXEFIGHTER; Initial Catalog=WebDB1; Integrated Security=SSPI"))
        {
            Connection.Open();
            SqlDataAdapter DataAdapter = new SqlDataAdapter();
            string QueryStringuser = "";
            QueryStringuser += "INSERT INTO userinfo  (login, password, email, sex, borndate) VALUES ('" + UserName.Text + "','" + Password.Text + "','" + EmailField.Text + "','" + SexField.Text + "','" + DateField.SelectedValue + "." + MonthField.SelectedValue + "." + YearField.SelectedValue + "');";
            DataAdapter.InsertCommand = new SqlCommand(QueryStringuser, Connection);
            DataAdapter.InsertCommand.ExecuteNonQuery();

            Connection.Close();
        }
        var manager = new UserManager();
        var user = new ApplicationUser() { UserName = UserName.Text };
        IdentityResult result = manager.Create(user, Password.Text);
        if (result.Succeeded)
        {
            IdentityHelper.SignIn(manager, user, isPersistent: false);
            IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);

            

                //userinfotable.login = UserName.Text;
                //userinfotable.password = Password.Text;
                //userinfotable.email = EmailField.Text;
                //userinfotable.sex = SexField.Text;
                //userinfotable.borndate = DateField.SelectedValue + "." + MonthField.SelectedValue + "." + YearField.SelectedValue;

                //userdb.userinfo.InsertOnSubmit(userinfotable);
                //userdb.SubmitChanges();
            }
        else
        {
            ErrorMessage.Text = result.Errors.FirstOrDefault();
        }
    }
}