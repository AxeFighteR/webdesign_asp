﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Update.aspx.cs" Inherits="Update" %>
<%@ PreviousPageType VirtualPath="/Default.aspx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center><b>Подача заявки на збір ПК</b><hr width=55%></center>
    <form action="MyResponse.asp" method="post">
    <table rules="none" style="width: 60%;" align="center">
        <tr>
            <td style="text-align: left; width: 22%; height: 31px">ПІБ</td>
            <td style="text-align: left; height: 40px">
                <asp:TextBox ID="pib" runat="server" Width="572px" required="required"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; width: 22%; height: 33px">E-mail</td>
            <td style="text-align: left; height: 40px">
                <asp:TextBox ID="mail" runat="server" Width="572px" required="required" CssClass="mail" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; width: 22%; height: 31px">Блок живлення</td>
            <td style="text-align: left; height: 40px">
                <asp:DropDownList ID="bp" runat="server" required="required" DataSourceID="SqlDataSource1" DataTextField="Назва_блоку_живлення" DataValueField="id_блоку_живлення" CssClass="form-control" >
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; width: 22%">Накопичувач</td>
            <td style="text-align: left; height: 40px">
                <asp:ListBox ID="drive" runat="server" required="required" DataSourceID="SqlDataSource2" DataTextField="Назва_накопичувача" DataValueField="id_накопичувача" SelectionMode="Multiple"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; width: 22%">Процесор</td>
            <td style="text-align: left; height: 40px">
                <asp:RadioButtonList ID="cpu" runat="server" required="required" DataSourceID="SqlDataSource3" DataTextField="Назва_процесора" DataValueField="id_процесора" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; height: 31px; width: 22%">ОС</td>
            <td style="text-align: left; height: 40px">
                <asp:CheckBoxList ID="os" runat="server" required="required" DataSourceID="SqlDataSource4" DataTextField="Назва_ОС" DataValueField="id_ОС" RepeatColumns="3" RepeatDirection="Horizontal">
                </asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; width: 22%; height: 40px;">Доставка</td>
            <td style="text-align: left; height: 40px">
                <asp:RadioButtonList ID="delivery" runat="server" required="required" DataSourceID="SqlDataSource5" DataTextField="Назва_доставки" DataValueField="id_доставки" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; width: 22%; height: 40px">Адреса</td>
            <td style="text-align: left; height: 40px">
                <asp:TextBox ID="address" required="required" CssClass="form-control" runat="server" Width="571px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; width: 22%">Дод. інформ.</td>
            <td style="text-align: left; height: 40px">
                <asp:TextBox ID="info" runat="server" Width="571px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; width: 22%; height: 7px;"></td>
            <td style="text-align: left; height: 7px"></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 22%">&nbsp;</td>
            <td style="text-align: left; height: 40px"><asp:Button ID="submit" runat="server" Text="Підтвердити оновлення" Width="351px" OnClick="submit_Click" OnClientClick="if ( ! UserUpdateConfirmation()) return false;"  />
                        <script type="text/javascript" language="javascript"> 
                        function UserDeleteConfirmation() {
                            if (confirm("Ви бажаєте оновити свою заявку?"))
                                return true;
                            else
                                return false;
                            }
                    </script>

&nbsp;<br />
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Очистити" Width="150px" />
&nbsp;<asp:Button ID="Button2" runat="server" PostBackUrl="~/Default.aspx" Text="Перегляд записів" Width="195px" UseSubmitBehavior="False" />
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:WebDB1ConnectionString %>" SelectCommand="SELECT * FROM [Блок_живлення]" OnSelecting="SqlDataSource1_Selecting"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:WebDB1ConnectionString %>" SelectCommand="SELECT * FROM [Накопичувач]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:WebDB1ConnectionString %>" SelectCommand="SELECT * FROM [Процесор]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:WebDB1ConnectionString %>" SelectCommand="SELECT * FROM [ОС]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:WebDB1ConnectionString %>" SelectCommand="SELECT * FROM [Доставка]"></asp:SqlDataSource>
            </td>
        </tr>
    </table>
        </form>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />

</asp:Content>

