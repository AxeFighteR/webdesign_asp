﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Lab5form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }

    protected void submit_Click(object sender, EventArgs e)
    {
        using (SqlConnection Connection = new SqlConnection(@"Data Source=AXEFIGHTER; Initial Catalog=WebDB1; Integrated Security=SSPI"))
        {
            Connection.Open();
            SqlDataAdapter DataAdapter = new SqlDataAdapter();
            //feedbacktodbDataContext db = new feedbacktodbDataContext();

            //Заявка table = new Заявка();
            //ОС_заявка tableos = new ОС_заявка();
            //Накопичувач_заявка tabledrive = new Накопичувач_заявка();

            //table.id_заявки = 2;
            //table.ПІБ = pib.Text;
            //table.Email = mail.Text;
            //table.Блок_живлення = Convert.ToInt32(bp.SelectedValue);
            //table.Процесор = Convert.ToInt32(cpu.SelectedValue);
            //table.Доставка = Convert.ToInt32(delivery.SelectedValue);
            //table.Адреса = address.Text;
            //table.Дод_інформ = info.Text;

            //db.Заявка.InsertOnSubmit(table);
            //db.SubmitChanges();

            string ZatytInsertQuery = "";
            ZatytInsertQuery = "INSERT INTO Заявка (ПІБ, Email, Блок_живлення, Процесор, Доставка, Адреса, Дод_інформ, edit_by, edit_time) VALUES ('" + pib.Text + "','" + mail.Text + "','" + Convert.ToInt32(bp.SelectedValue) + "','" + Convert.ToInt32(cpu.SelectedValue) + "','" + Convert.ToInt32(delivery.SelectedValue) + "','" + address.Text + "','" + info.Text + "','" + Context.User.Identity.Name.ToString() + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "');";
            DataAdapter.InsertCommand = new SqlCommand(ZatytInsertQuery, Connection);
            DataAdapter.InsertCommand.ExecuteNonQuery();
            //----------------------------------------------------------------

            SqlCommand CommandQuery = new SqlCommand("SELECT id_заявки FROM Заявка WHERE ПІБ='" + pib.Text + "' AND Адреса='" + address.Text + "'  ", Connection);
            string ID_req = " ";           
            SqlDataReader DataReader = CommandQuery.ExecuteReader();
            if (DataReader.HasRows)
            {
                DataReader.Read();
                ID_req = (DataReader["id_заявки"].ToString());
                DataReader.Close();
            }
            else
            {
                DataReader.Close();
            }
            //----------------------------------------------------------------

            string QueryString1 = "";
            string QueryString2 = "";

            for (int i = 0; i < drive.Items.Count; i++)
            {
                if (drive.Items[i].Selected)
                {
                    QueryString1 += "INSERT INTO Накопичувач_заявка (id_заявки, id_накопичувача) VALUES ('" + ID_req + "','" + (i+1) + "');";
                }
            }
            
            DataAdapter.InsertCommand = new SqlCommand(QueryString1, Connection);
            DataAdapter.InsertCommand.ExecuteNonQuery();

            for (int i = 0; i < os.Items.Count; i++)
            {
                if (os.Items[i].Selected)
                {
                    QueryString2 += "INSERT INTO ОС_заявка (id_заявки, id_ОС) VALUES ('" + ID_req + "','" + (i + 1) + "');";
                }
            }
            DataAdapter.InsertCommand = new SqlCommand(QueryString2, Connection);
            DataAdapter.InsertCommand.ExecuteNonQuery();

            /*  for (int i = 0; i < drive.Items.Count; i++)
              {
                  feedbacktodbDataContext db0 = new feedbacktodbDataContext();
                  if (drive.Items[i].Selected)
                  {          

                          tabledrive.id_заявки = Convert.ToInt32(ID_req);
                          tabledrive.id_накопичувача = i;

                      db0.Накопичувач_заявка.InsertOnSubmit(tabledrive);
                      db0.Накопичувач_заявка.Count(); 


                  }
                  db0.SubmitChanges();
              }*/



            Connection.Close();
        }
        Response.Redirect("~/Default.aspx");
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        pib.Text = "";
        mail.Text = "";
        bp.ClearSelection();
        drive.ClearSelection();
        cpu.ClearSelection();
        os.ClearSelection();
        delivery.ClearSelection();
        address.Text = "";
        info.Text = "";
    }

}