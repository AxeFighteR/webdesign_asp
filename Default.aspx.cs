﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        using (SqlConnection Connection = new SqlConnection(@"Data Source=AXEFIGHTER; Initial Catalog=WebDB1; Integrated Security=SSPI"))
        {
            Connection.Open();
            SqlCommand CommandQuery = new SqlCommand("SELECT * FROM Заявка", Connection);
            SqlDataReader DataReader = CommandQuery.ExecuteReader();
            if (DataReader.HasRows)
            {
                Label1.Text = "Перегляд таблиці з внесеними даними:";
                if (!Context.User.Identity.IsAuthenticated)
                {
                   GridView1.Columns[11].Visible = false;
                   GridView1.Columns[12].Visible = false;
                }
                else
                {
                   GridView1.Columns[11].Visible = true;
                   GridView1.Columns[12].Visible = true;
                }
            
                
            }
            else
            {
                Label1.Text = "Немає даних для відображення! Ви можете заповнити форму опитування, натиснувши на кнопку нижче";
                DataReader.Close();
            }
            Connection.Close();
        }

    }


    protected void BtnUserDelete_Click(object sender, EventArgs e)
    {
        using(SqlConnection Connection = new SqlConnection(@"Data Source=AXEFIGHTER; Initial Catalog=WebDB1; Integrated Security=SSPI"))
        {
            Connection.Open();
            SqlDataAdapter DataAdapter = new SqlDataAdapter();

            DataAdapter.DeleteCommand = new SqlCommand("DELETE FROM Накопичувач_заявка WHERE id_заявки=" + GridView1.SelectedValue, Connection);
            DataAdapter.DeleteCommand.ExecuteNonQuery();

            DataAdapter.DeleteCommand = new SqlCommand("DELETE FROM ОС_заявка WHERE id_заявки=" + GridView1.SelectedValue, Connection);
            DataAdapter.DeleteCommand.ExecuteNonQuery();

            DataAdapter.DeleteCommand = new SqlCommand("DELETE FROM Заявка WHERE id_заявки=" + GridView1.SelectedValue, Connection);
            DataAdapter.DeleteCommand.ExecuteNonQuery();

            Response.Redirect("~/Default.aspx");
        }
    }
    public int UpdSelRow
    {
        get
        {
            return Convert.ToInt32(GridView1.SelectedValue);
        }
    }


    protected void BtnUserUpdate_Click(object sender, EventArgs e)
    {
        //TextBox1.Text = GridView1.SelectedValue.ToString();
        Response.Redirect("Update.aspx?UpdSelItem=" + GridView1.SelectedValue.ToString());

    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if(e.Row.Cells[8].Text == Context.User.Identity.Name)
            {
                e.Row.Style.Add(HtmlTextWriterStyle.BackgroundColor,
                        System.Drawing.ColorTranslator.ToHtml(
                           System.Drawing.Color.Lime));
                Button1.Visible = false;
                e.Row.Cells[11].Visible = true;
                e.Row.Cells[12].Visible = true;

            }
            else
            {
                e.Row.Cells[11].Visible = false;
                e.Row.Cells[12].Visible = false;



            }

        }
    }

    
}