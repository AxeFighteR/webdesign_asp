﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WEBtest.Startup))]
namespace WEBtest
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
