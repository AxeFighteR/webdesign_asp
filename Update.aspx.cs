﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Update : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            using (SqlConnection Connection = new SqlConnection(@"Data Source=AXEFIGHTER; Initial Catalog=WebDB1; Integrated Security=SSPI"))
            {
                Connection.Open();
                SqlDataAdapter DataAdapter = new SqlDataAdapter();
                feedbacktodbDataContext db = new feedbacktodbDataContext();

                Заявка table = new Заявка();
                ОС_заявка tableos = new ОС_заявка();
                Накопичувач_заявка tabledrive = new Накопичувач_заявка();
                int Selection = Convert.ToInt32(Request.QueryString["UpdSelItem"]);

                String UpdValue = "";

                //-----------тащим из базы имя по ключу
                SqlCommand CommandQuery1 = new SqlCommand("SELECT ПІБ FROM Заявка WHERE id_заявки='" + Selection + "'", Connection);
                SqlDataReader DataReader1 = CommandQuery1.ExecuteReader();
                if (DataReader1.HasRows)
                {
                    DataReader1.Read();
                    UpdValue = (DataReader1["ПІБ"].ToString());
                    DataReader1.Close();
                }
                else
                {
                    DataReader1.Close();
                }
                pib.Text = UpdValue;

                //------------тащим из базы mail по ключу
                SqlCommand CommandQuery2 = new SqlCommand("SELECT Email FROM Заявка WHERE id_заявки='" + Selection + "'", Connection);
                SqlDataReader DataReader2 = CommandQuery2.ExecuteReader();
                if (DataReader2.HasRows)
                {
                    DataReader2.Read();
                    UpdValue = (DataReader2["Email"].ToString());
                    DataReader2.Close();
                }
                else
                {
                    DataReader2.Close();
                }
                mail.Text = UpdValue;

                //------------тащим из базы БП по ключу
                SqlCommand CommandQuery3 = new SqlCommand("SELECT Блок_живлення FROM Заявка WHERE id_заявки='" + Selection + "'", Connection);
                SqlDataReader DataReader3 = CommandQuery3.ExecuteReader();
                if (DataReader3.HasRows)
                {
                    DataReader3.Read();
                    UpdValue = (DataReader3["Блок_живлення"].ToString());
                    DataReader3.Close();
                }
                else
                {
                    DataReader3.Close();
                }
                bp.SelectedValue = UpdValue;

                //------------тащим из базы процессор по ключу
                SqlCommand CommandQuery4 = new SqlCommand("SELECT Процесор FROM Заявка WHERE id_заявки='" + Selection + "'", Connection);
                SqlDataReader DataReader4 = CommandQuery4.ExecuteReader();
                if (DataReader4.HasRows)
                {
                    DataReader4.Read();
                    UpdValue = (DataReader4["Процесор"].ToString());
                    DataReader4.Close();
                }
                else
                {
                    DataReader4.Close();
                }
                cpu.SelectedValue = UpdValue;

                //------------тащим из базы адрес по ключу
                SqlCommand CommandQuery5 = new SqlCommand("SELECT Доставка FROM Заявка WHERE id_заявки='" + Selection + "'", Connection);
                SqlDataReader DataReader5 = CommandQuery5.ExecuteReader();
                if (DataReader5.HasRows)
                {
                    DataReader5.Read();
                    UpdValue = (DataReader5["Доставка"].ToString());
                    DataReader5.Close();
                }
                else
                {
                    DataReader5.Close();
                }
                delivery.SelectedValue = UpdValue;

                //------------тащим из базы адрес по ключу
                SqlCommand CommandQuery6 = new SqlCommand("SELECT Адреса FROM Заявка WHERE id_заявки='" + Selection + "'", Connection);
                SqlDataReader DataReader6 = CommandQuery6.ExecuteReader();
                if (DataReader6.HasRows)
                {
                    DataReader6.Read();
                    UpdValue = (DataReader6["Адреса"].ToString());
                    DataReader6.Close();
                }
                else
                {
                    DataReader6.Close();
                }
                address.Text = UpdValue;


                //------------тащим из базы доп инфо по ключу
                SqlCommand CommandQuery7 = new SqlCommand("SELECT Дод_інформ FROM Заявка WHERE id_заявки='" + Selection + "'", Connection);
                SqlDataReader DataReader7 = CommandQuery7.ExecuteReader();
                if (DataReader7.HasRows)
                {
                    DataReader7.Read();
                    UpdValue = (DataReader7["Дод_інформ"].ToString());
                    DataReader7.Close();
                }
                else
                {
                    DataReader7.Close();
                }
                info.Text = UpdValue;




                Connection.Close();
            }
        }

    }

    protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    


    protected void submit_Click(object sender, EventArgs e)
    {
        using (SqlConnection Connection = new SqlConnection(@"Data Source=AXEFIGHTER; Initial Catalog=WebDB1; Integrated Security=SSPI"))
        {
            Connection.Open();
            SqlDataAdapter DataAdapter = new SqlDataAdapter();

            int Selection1 = Convert.ToInt32(Request.QueryString["UpdSelItem"]);

            DataAdapter.DeleteCommand = new SqlCommand("DELETE FROM Накопичувач_заявка WHERE id_заявки=" + Selection1, Connection);
            DataAdapter.DeleteCommand.ExecuteNonQuery();

            DataAdapter.DeleteCommand = new SqlCommand("DELETE FROM ОС_заявка WHERE id_заявки=" + Selection1, Connection);
            DataAdapter.DeleteCommand.ExecuteNonQuery();

            DataAdapter.DeleteCommand = new SqlCommand("DELETE FROM Заявка WHERE id_заявки=" + Selection1, Connection);
            DataAdapter.DeleteCommand.ExecuteNonQuery();

            string ZatytUpdateQuery = "";
            ZatytUpdateQuery = "INSERT INTO Заявка (ПІБ, Email, Блок_живлення, Процесор, Доставка, Адреса, Дод_інформ, edit_by, edit_time) VALUES ('" + pib.Text + "','" + mail.Text + "','" + Convert.ToInt32(bp.SelectedValue) + "','" + Convert.ToInt32(cpu.SelectedValue) + "','" + Convert.ToInt32(delivery.SelectedValue) + "','" + address.Text + "','" + info.Text + "','"+ Context.User.Identity.Name.ToString() +"','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "');";
            DataAdapter.InsertCommand = new SqlCommand(ZatytUpdateQuery, Connection);
            DataAdapter.InsertCommand.ExecuteNonQuery();
            //----------------------------------------------------------------

            SqlCommand CommandQuery = new SqlCommand("SELECT id_заявки FROM Заявка WHERE ПІБ='" + pib.Text + "' AND Адреса='" + address.Text + "'  ", Connection);
            string ID_req = " ";
            SqlDataReader DataReader = CommandQuery.ExecuteReader();
            if (DataReader.HasRows)
            {
                DataReader.Read();
                ID_req = (DataReader["id_заявки"].ToString());
                DataReader.Close();
            }
            else
            {
                DataReader.Close();
            }
            //----------------------------------------------------------------

            string QueryString1 = "";
            string QueryString2 = "";

            for (int i = 0; i < drive.Items.Count; i++)
            {
                if (drive.Items[i].Selected)
                {
                    QueryString1 += "INSERT INTO Накопичувач_заявка (id_заявки, id_накопичувача) VALUES ('" + ID_req + "','" + (i + 1) + "');";
                }
            }

            DataAdapter.InsertCommand = new SqlCommand(QueryString1, Connection);
            DataAdapter.InsertCommand.ExecuteNonQuery();

            for (int i = 0; i < os.Items.Count; i++)
            {
                if (os.Items[i].Selected)
                {
                    QueryString2 += "INSERT INTO ОС_заявка (id_заявки, id_ОС) VALUES ('" + ID_req + "','" + (i + 1) + "');";
                }
            }
            DataAdapter.InsertCommand = new SqlCommand(QueryString2, Connection);
            DataAdapter.InsertCommand.ExecuteNonQuery();

     

            Connection.Close();
        }
        Response.Redirect("~/Default");
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        pib.Text = "";
        mail.Text = "";
        bp.ClearSelection();
        drive.ClearSelection();
        cpu.ClearSelection();
        os.ClearSelection();
        delivery.ClearSelection();
        address.Text = "";
        info.Text = "";
    }


}